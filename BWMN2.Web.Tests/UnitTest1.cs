using NUnit.Framework;
using System.Threading.Tasks;

namespace BWMN2.Web.Tests
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public async Task Test1()
        {
            var db = new Db();
            await db.InitializeMaps();
            var maps = await db.RetrieveMaps();


        }

        [Test]
        public async Task Test2()
        {
            var db = new Db();
            await db.InitializeBlogs();
           
            var blogs = await db.RetrieveBlogs();
        }
    }
}