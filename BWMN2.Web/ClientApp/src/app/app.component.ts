import { AuthenticationResult, InteractionType, PopupRequest, RedirectRequest } from '@azure/msal-browser';
import { MsalBroadcastService, MsalGuardConfiguration, MsalService, MSAL_GUARD_CONFIG } from '@azure/msal-angular';
import { Component, Inject, OnInit } from '@angular/core';
import { PageLayoutService } from './services/common/app-page-layout.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  currentComponent: any = null;

  constructor(private appData: PageLayoutService) {

  }

  getPageContainerStyle(): string {
    if (this.currentComponent == null) return "container";

      if (!!this.currentComponent.containerStyle)
        return this.currentComponent.containerStyle
    else return 'container';
  }

  onRouterOutletActivate(event: any) {
    console.log(event);
    this.currentComponent = event;
  }
}
