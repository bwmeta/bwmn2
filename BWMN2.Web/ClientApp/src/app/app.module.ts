import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClient, HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { NavMenuComponent } from './routing/nav-menu/nav-menu.component';
import { HomeComponent } from './home/home.component'; 

import { MsalModule, MsalService, MsalGuard, MsalBroadcastService, MsalRedirectComponent, MSAL_INSTANCE, MsalGuardConfiguration, MSAL_GUARD_CONFIG } from "@azure/msal-angular";
import { PublicClientApplication, InteractionType, IPublicClientApplication } from "@azure/msal-browser"; 
 
 
import { EditViewMapComponent } from './maps/create/edit-view-map.component';
import { msalConfig } from './services/auth/auth-config';
import { AppRoutingModule } from './routing/app-routing.module';
import { CounterComponent } from './other/counter/counter.component';
import { FetchDataComponent } from './other/fetch-data/fetch-data.component';
import { RestrictedPageComponent } from './other/restricted-page/restricted-page.component';
import { PublicPageComponent } from './other/public-page/public-page.component'; 
import { CKEditorModule } from '@ckeditor/ckeditor5-angular'; 
import { MapGalleryComponent } from './maps/map-gallery/map-gallery.component';
import { JsonDateInterceptorService } from './services/json-date-interceptor.service';
import { EditBlogComponent } from './blogs/edit-blog/edit-blog.component';
import { RenderBlogComponent } from './blogs/render-blog/render-blog.component';
import { LoaderOverlayComponent } from './common/loader-overlay/loader-overlay.component';
import { ViewMapComponent } from './maps/view-map/view-map.component';
import { MapStudioComponent } from './maps/studio/map-studio.component';
import { BaseComponent } from './common/base/base.component';
import { PageLayoutService } from './services/common/app-page-layout.service';



/**
 * Here we pass the configuration parameters to create an MSAL instance.
 * For more info, visit: https://github.com/AzureAD/microsoft-authentication-library-for-js/blob/dev/lib/msal-angular/docs/v2-docs/configuration.md
 */
export function MSALInstanceFactory(): IPublicClientApplication {
  return new PublicClientApplication(msalConfig);  
}

/**
 * Set your default interaction type for MSALGuard here. If you have any
 * additional scopes you want the user to consent upon login, add them here as well.
 */
export function MSALGuardConfigFactory(): MsalGuardConfiguration {
  return {
    
    interactionType: InteractionType.Redirect,
  };
}


@NgModule({
  declarations: [
    AppComponent,
    PublicPageComponent,
    RestrictedPageComponent,
    NavMenuComponent,
    HomeComponent,
    CounterComponent,
    FetchDataComponent,
    EditViewMapComponent, 
    MapGalleryComponent, EditBlogComponent, RenderBlogComponent, LoaderOverlayComponent, ViewMapComponent, MapStudioComponent
   
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MsalModule,
    HttpClientModule,
    FormsModule,
    CKEditorModule
  ],
  providers: [ 
    {
      provide: MSAL_INSTANCE,
      useFactory: MSALInstanceFactory
    },
    {
      provide: MSAL_GUARD_CONFIG,
      useFactory: MSALGuardConfigFactory
    },
    MsalService,
    MsalGuard,
    MsalBroadcastService,
    { provide: HTTP_INTERCEPTORS, useClass: JsonDateInterceptorService, multi: true }
  ],
  bootstrap: [AppComponent, MsalRedirectComponent]
})
export class AppModule { }
