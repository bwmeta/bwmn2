import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Route, Router } from '@angular/router';
import { BlogModel } from '../../models/blog-model';
import { BlogApiService } from '../../services/api/blog-api-service.service';
import { SessionStateService } from '../../services/session-state.service';



@Component({
  selector: 'app-edit-blog',
  templateUrl: './edit-blog.component.html',
  styleUrls: ['./edit-blog.component.css']
})
export class EditBlogComponent implements OnInit {
  
  public id: string;
  blogModel: BlogModel;
  isLoaded: boolean = false;

  constructor(private route: ActivatedRoute, private blogApi: BlogApiService, private router: Router, private sessionState: SessionStateService) { }

  async ngOnInit() {
    this.sessionState.setIsLoading(true);
    this.route.params.subscribe((params: Params) => this.id = params['id']);
    this.blogModel = await this.blogApi.get(this.id);
    this.isLoaded = true;
    this.sessionState.setIsLoading(false);
  }

 
  async save(): Promise<void> {
    await this.blogApi.saveBlogAsync(this.blogModel);
  }

  async delete(): Promise<void> {
    await this.blogApi.deleteBlogAsync(this.blogModel);
    this.router.navigateByUrl("/");
  }
}
