import { Component, Input, OnInit } from '@angular/core';
import { BlogModel } from '../../models/blog-model';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
@Component({
  selector: 'app-render-blog',
  templateUrl: './render-blog.component.html',
  styleUrls: ['./render-blog.component.css']
})
export class RenderBlogComponent implements OnInit {
  public Editor = ClassicEditor;
  @Input() blogModel: BlogModel;
  @Input() disabled: boolean = false;



  constructor() { }

  ngOnInit() {
  }
  public onReady(editor) {
  }


}
