import { Component, OnInit } from '@angular/core';
import { PageLayoutService } from '../../services/common/app-page-layout.service';
 
export class BaseComponent  {

  componentLayout: string;
  constructor(protected appData: PageLayoutService) {
  }
   
}
