import { Component, OnInit } from '@angular/core';
import { SessionStateService } from '../../services/session-state.service';

@Component({
  selector: 'app-loader-overlay',
  templateUrl: './loader-overlay.component.html',
  styleUrls: ['./loader-overlay.component.css']
})
export class LoaderOverlayComponent implements OnInit {

  constructor(private sessionState: SessionStateService) { }

  ngOnInit() {
  }

  getIsLoading(): boolean {
    return this.sessionState.getIsLoading();
  }
}
