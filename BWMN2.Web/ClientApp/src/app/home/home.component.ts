import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MsalService } from '@azure/msal-angular';

import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { BlogModel } from '../models/blog-model';
import { CreateBlogModel } from '../models/create-blog-model';
import { BlogApiService } from '../services/api/blog-api-service.service';
import { SessionStateService } from '../services/session-state.service';

@Component({
  styleUrls: ['./home.component.css'],
  selector: 'app-home',
  templateUrl: './home.component.html',
})
export class HomeComponent implements OnInit {
  constructor(private blogApi: BlogApiService, private authService: MsalService, private router: Router, private sessionState: SessionStateService) { }
  async ngOnInit(): Promise<void> {
    this.sessionState.setIsLoading(true);
    await this.listBlogs();
    this.sessionState.setIsLoading(false);
  }
  public Editor = ClassicEditor;

  blogSubjectInput: string = "";
  blogMessageInput: string = "";

  blogs: BlogModel[];

  async listBlogs(): Promise<void> {
    this.blogs = await this.blogApi.listBlogsAsync();
  }

  async createBlog(): Promise<void> {
    var blog = new CreateBlogModel();
    blog.messageBody = this.blogMessageInput;
    blog.subject = this.blogSubjectInput;
    await this.blogApi.submitNewBlogAsync(blog);
    await this.listBlogs();
  }

  public onReady(editor) { 
  }

  isLoggedIn(): boolean {
    return this.authService.instance.getActiveAccount() != null
  }

  datesAreDifferent(date1: Date, date2: Date): boolean {
    
    return date1.getTime() !== date2.getTime();
  }

  editBlog(blog: BlogModel): void {
    this.router.navigate([`blogs/edit`, blog.id]);
  }
}
