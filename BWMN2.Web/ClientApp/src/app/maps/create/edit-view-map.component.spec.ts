import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditViewMapComponent } from './edit-view-map.component';

describe('CreateNewMapComponent', () => {
  let component: EditViewMapComponent;
  let fixture: ComponentFixture<EditViewMapComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditViewMapComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditViewMapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
