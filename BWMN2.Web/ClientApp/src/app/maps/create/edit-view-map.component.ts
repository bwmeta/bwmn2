import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AfterViewInit, Component, ElementRef, Inject, Input, OnChanges, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import { MsalService } from '@azure/msal-angular';
import { AccountInfo, AuthenticationResult, IPublicClientApplication } from '@azure/msal-browser';
import { CommonImageToolsService } from '../../services/auth/images/common-image-tools-service';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { ChangeEvent } from '@ckeditor/ckeditor5-angular/ckeditor.component';
import { MapModel, MapVariantModel, MapVersionModel } from '../../models/map-model';
import { MapsApiService } from '../../services/api/maps-api-service.service';
import { CreateMapModel } from '../../models/create-map-model'; 
import { AppDataService, CompatibilityMode, GameType, MapStatus, MapType, TileSet } from '../../services/common/app-data.service';



@Component({
  selector: 'app-edit-view-map',
  templateUrl: './edit-view-map.component.html',
  styleUrls: ['./edit-view-map.component.css']
})
export class EditViewMapComponent implements OnInit, OnChanges  {

  constructor(private imageTools: CommonImageToolsService, private dataService: AppDataService,
    private http: HttpClient, @Inject('BASE_URL') private baseUrl: string, private msalService: MsalService, private mapsApi: MapsApiService) {

  }
  ngOnChanges(changes: SimpleChanges): void { 
  }

  @Input() selectedMap: MapModel;
  @Input() selectedVariant: MapVariantModel;
  @Input() selectedVersion: MapVersionModel;
  
  public Editor = ClassicEditor;
  mapThumbnailUpload: File | null = null;
  mapFileUpload: File | null = null;
  @ViewChild("refImage", { static: false }) mapThumbnailImage: ElementRef<HTMLImageElement>;
  mapName: string = "";
  editorInput: string = "test2";
  maps: MapModel[];
  

  loaded: boolean = false;

  
 
  selectedTileset: TileSet = null;
  selectedGameTypes: GameType[] = [];
  selectedMapTypes: MapType[] = [];
  selectedCompatibilityMode: CompatibilityMode = null;
  selectedMapStatus: MapStatus = null;

  descMode: string = "map";

  get mapStatuses(): MapStatus[]  {
    return this.dataService.mapStatuses;
  }
  get compatibilityModes(): CompatibilityMode[] {
    return this.dataService.compatibilityModes;
  }
  get mapTypes(): MapType[] {
    return this.dataService.mapTypes;
  }
  get gameTypes(): GameType[] { 
    return this.dataService.gameTypes;
  }
  get tilesets(): TileSet[] {
    return this.dataService.tilesets;
  }

  async ngOnInit() {
    this.maps = await this.mapsApi.listMapsAsync();

    if (this.selectedMap == null) {
      this.selectedTileset = this.dataService.tilesets[0];;
      this.selectedGameTypes = [this.dataService.gameTypes[0]];
      this.selectedMapTypes = [this.dataService.mapTypes[0]];
      this.selectedCompatibilityMode = this.dataService.compatibilityModes[0];
      this.selectedMapStatus = this.dataService.mapStatuses[0];
      this.selectedMap = this.maps[0]; 

    }
    else {
     
    } 

    this.loaded = true;
  }
   
  isDescMode(mode: string): boolean {
    return this.descMode == mode;
  }
  setDescMode(mode: string): void {
    this.descMode = mode;
  }

  public onChange({ editor }: ChangeEvent) {
    const data = editor.getData();
  }

  async submitNewMap() {
    //this.map.version.mapTypeIds.length = 0;
    //this.selectedMapTypes.forEach(a => this.map.version.mapTypeIds.push(a.id));

    //this.map.version.gameTypeIds.length = 0;
    //this.selectedGameTypes.forEach(a => this.map.version.gameTypeIds.push(a.id));

    //this.map.version.tilesetId = this.selectedTileset.id;
    //this.map.version.statusId = this.selectedMapStatus.id;
    //this.map.version.compatibilityId = this.selectedCompatibilityMode.id;

    //await this.mapsApi.submitNewMapAsync(this.map);
    //this.map.reset();
  }
  async handleMapThumbnailInput(files: FileList) {
    this.mapThumbnailUpload = files.item(0);
    var optImage = await this.imageTools.resizeImage(this.mapThumbnailUpload);
    var canvas = optImage.toCanvas();
    var dataUrl = canvas.toDataURL();
    this.imageTools.renderDataUrlAsync(this.mapThumbnailImage.nativeElement, dataUrl);
    this.selectedVersion.mapThumbnailDataUrl = dataUrl;

  }
  async handleMapFileInput(files: FileList) {
    this.mapFileUpload = files.item(0);
    var fr = new FileReader();
    fr.readAsDataURL(this.mapFileUpload);

    fr.onload = (a) => {
      this.selectedVersion.mapFileBlob = fr.result as string;
    };

  }
  selectTileset(tileset: TileSet) { 
    this.selectedVersion.tilesetId = tileset.id;
  }
  getSelectedTileset(tileset: TileSet): boolean {
    return this.selectedVersion.tilesetId == tileset.id;
  }
  toggleGameType(gameType: GameType): void {
    if (this.hasGameType(gameType)) {
      this.selectedVersion.gameTypeIds.splice(this.selectedVersion.gameTypeIds.indexOf(gameType.id), 1);
    }
    else {
      this.selectedVersion.gameTypeIds.push(gameType.id);
    }
  }
  hasGameType(gameType: GameType): boolean {
    return this.selectedVersion.gameTypeIds.includes(gameType.id);
  }
  toggleMapType(mapType: MapType): void {
    if (this.hasMapType(mapType)) {
      this.selectedVersion.mapTypeIds.splice(this.selectedVersion.mapTypeIds.indexOf(mapType.id), 1);
    }
    else {
      this.selectedVersion.mapTypeIds.push(mapType.id);
    }
  }
  hasMapType(mapType: MapType): boolean {
    return this.selectedVersion.mapTypeIds.includes(mapType.id);
  }

  isStatusSelected(mapStatus: MapStatus): boolean { 
    return this.selectedVersion.statusId == mapStatus.id;
  }

  isCompatibilitySelected(comp: CompatibilityMode): boolean {
    return this.selectedVersion.compatibilityId == comp.id;
  }
}

