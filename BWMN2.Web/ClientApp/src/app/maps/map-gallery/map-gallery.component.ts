import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MapModel } from '../../models/map-model';
import { MapsApiService } from '../../services/api/maps-api-service.service';

@Component({
  selector: 'app-map-gallery',
  templateUrl: './map-gallery.component.html',
  styleUrls: ['./map-gallery.component.css']
})
export class MapGalleryComponent implements OnInit {

  constructor(private mapsApi: MapsApiService, private router: Router) { }

  maps: MapModel[];
  selectedMap: MapModel;

  async ngOnInit() { 
    this.maps = await this.mapsApi.listMapsAsync();
  }
  view(map: MapModel) {
    this.selectedMap = map;
  }
  onBack(event): void {
    this.selectedMap = null; 
  }
  getMapThumbnailUrl(map: MapModel) {
    return map.variants[0].versions[0].mapThumbnailDataUrl;
  }
}
