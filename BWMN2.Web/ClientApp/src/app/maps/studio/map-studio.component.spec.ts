import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MapStudioComponent } from './map-studio.component';

describe('ForgeComponent', () => {
  let component: MapStudioComponent;
  let fixture: ComponentFixture<MapStudioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MapStudioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MapStudioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
