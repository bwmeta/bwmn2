import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { BaseComponent } from '../../common/base/base.component';
import { MapModel, MapVariantModel, MapVersionModel } from '../../models/map-model';
import { MapsApiService } from '../../services/api/maps-api-service.service';
import { PageLayoutService } from '../../services/common/app-page-layout.service';

@Component({
  selector: 'app-map-studio',
  templateUrl: './map-studio.component.html',
  styleUrls: ['./map-studio.component.css']
})
export class MapStudioComponent implements OnInit {
  public containerStyle: string = "container-fluid";
  studioModeLoadMap: string = "studio-view";
  studioModeDefault: string = "studio-edit"; 
  pageMode: string = this.studioModeDefault;
  mapId: string = null;

  maps: MapModel[];
  selectedMap: MapModel;
  selectedVariant: MapVariantModel;
  selectedVersion: MapVersionModel;
  isLoaded: boolean = false;
  isMenuVisible: boolean = true;

  constructor(private route: ActivatedRoute, private mapsApi: MapsApiService) {
    /*  
     *  Show a "View" mode to start (this is what any user can see, plus some collaborator-specific info)
     *  Button to switch between view and edit
     *  Creating a new map is integrated into the map forge
     *  ability to add new variants without createing a whole new map each time
     *  ability to add new versions without creating a whole new map each time
     *  
     */
  }

  async ngOnInit() {
    this.route.params.subscribe((params: Params) => this.mapId = params['id']);
    this.maps = await this.mapsApi.listMapsAsync();

    if (this.mapId == null) { 
      this.pageMode = this.studioModeDefault;
      this.setSelectedMap(this.maps[0]);
    }
    else { 
      this.pageMode = this.studioModeLoadMap;
      this.setSelectedMap(this.maps.find(a => a.id == this.mapId));
    }
     
    this.isLoaded = true;
  }

  getFirstMapThumbnail(map: MapModel): string {
    return map.variants[0].versions[0].mapThumbnailDataUrl;
  }


  setSelectedMap(map: MapModel): void {
    this.selectedMap = map;
    this.selectedVariant = this.selectedMap.variants[0];
    this.selectedVersion = this.selectedVariant.versions[0];
  }

  isMapSelected(map: MapModel): boolean {
    return this.selectedMap == map;
  }

  getSelectedMapVariants(): MapVariantModel[] {
    return this.selectedMap.variants || [];
  }

  setSelectedVariant(variant: MapVariantModel): void {
    this.selectedVariant = variant;
  }

  isVariantSelected(variant: MapVariantModel): boolean {
    return this.selectedVariant == variant;
  }
  

  getSelectedVariantVersions(): MapVersionModel[] {
    return this.selectedVariant.versions || [];
  } 
  isVersionSelected(version: MapVersionModel): boolean {
    return this.selectedVersion == version;
  }
  setSelectedVersion(version: MapVersionModel): void {
    this.selectedVersion = version;
  }

  toggleExpand(): void {
    this.isMenuVisible = !this.isMenuVisible;
  }

  isStudioMode(): boolean {
    return this.pageMode == this.studioModeDefault
      || this.pageMode == this.studioModeLoadMap;
  }
}
