import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { MapModel, MapVersionModel } from '../../models/map-model';
import { MapsApiService } from '../../services/api/maps-api-service.service';
import { SessionStateService } from '../../services/session-state.service';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { AppDataService, CompatibilityMode, GameType, MapStatus, MapType, TileSet } from '../../services/common/app-data.service';


@Component({
  selector: 'app-view-map',
  templateUrl: './view-map.component.html',
  styleUrls: ['./view-map.component.css']
})
export class ViewMapComponent implements OnInit {

  @Input() map: MapModel;
  @Output() back = new EventEmitter<string>();
  isLoaded: boolean = false;
  public Editor = ClassicEditor;

  newMessage: string = "";
  isShowingMapThumbnail: boolean = false;

  constructor(private appDataService: AppDataService, private route: ActivatedRoute, private mapApi: MapsApiService, private router: Router, private sessionState: SessionStateService) { }

  async ngOnInit() {  
    this.map = await this.mapApi.get(this.map.id);
    this.isLoaded = true; 
    
  }

  public onReady(editor) {
  }


  clickBack(): void {
    this.back.emit("test"); 
  }
  edit(): void {
    this.router.navigateByUrl("maps/studio/" + this.map.id);
  }

  getMapThumbnail(): string {
    return this.getLatestVersion().mapThumbnailDataUrl;
  }
  getLatestVersion(): MapVersionModel {
    return this.map.variants[0].versions[0];
  }
  getTileSet(): TileSet {
    return this.appDataService.toTilesetData(this.getLatestVersion().tilesetId);
  }

  getGameTypes(): GameType[] {
    return this.appDataService.toGameTypeData(this.getLatestVersion().gameTypeIds);
  }

  getMapTypes(): MapType[] {
    return this.appDataService.toMapTypeData(this.getLatestVersion().mapTypeIds);
  }

  getStatus(): MapStatus {
    var statusModel = this.appDataService.toMapStatusData(this.getLatestVersion().statusId);
    return statusModel;
  }

  getCompatibility(): CompatibilityMode {
    var compatibilityModel = this.appDataService.toCompatibilityData(this.getLatestVersion().compatibilityId);
    return compatibilityModel;
  }

  getHeight(): number {
    return this.getLatestVersion().height;
  }

  getWidth(): number {
    return this.getLatestVersion().width;
  }

  getCreatedDate(): Date {
    return this.getLatestVersion().createdDate;
  }

  getModifiedDate(): Date {
    return this.getLatestVersion().modifiedDate;
  }

  viewMapThumbnail(value: boolean): void {
    this.isShowingMapThumbnail = value;
  }
}
