export class BlogModel {
  id: string; 
  messageBody: string;
  createdDate: Date;
  modifiedDate: Date;
  blogType: string;
  subject: string;
}
