import { MapVersionModel } from "./map-model";

export class CreateMapModel {
  displayName: string = "";
  description: string = "";
  

  version: MapVersionModel;

  reset(): void {
    this.displayName = "";
    this.description = "";

    this.version.reset();
  }
}

