import { CollaboratorModel } from "./collaborator";

export class MapModel {
  id: string;
  displayName: string;
  description: string; 

  variants: MapVariantModel[];
  createdDate: Date;
  collaborators: CollaboratorModel[] = [];
}
export class MapVariantModel {
  displayName: string = "";
  versions: MapVersionModel[] = [];
}
export class MapVersionModel {
  id: string = "";
  mapThumbnailDataUrl: string = "";
  mapFileBlob: string = "";
  versionNumber: number = 0.1;
  description: string = "";

  tilesetId: number = 10;
  gameTypeIds: number[] = [10];  //1v1, 2v2, 3v3, 2v2v2v2, FFA etc.
  mapTypeIds: number[] = [10];//melee, observer, training, no FOW etc.
  statusId: number = 10; //alpha/WIP, beta/playtest, final
  compatibilityId: number = 10; //compatibility tag (1.22+ vs. 1.16)?

  tags: string[] = []; 
  height: number = 128;
  width: number = 128;

  createdDate: Date;
  modifiedDate: Date;

  reset(): void {
    this.id = "";
    this.mapThumbnailDataUrl = "";
    this.mapFileBlob = "";
    this.versionNumber = 0.1;
    this.description = "";
    this.tilesetId = 10;
    this.gameTypeIds = [10];
    this.mapTypeIds = [10];
    this.statusId = 10;
    this.compatibilityId = 10;

    this.tags.length = 0;
    this.height = 128;
    this.width = 128;
    
  }
}
