 
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';  
import { EditBlogComponent } from '../blogs/edit-blog/edit-blog.component';
import { HomeComponent } from '../home/home.component';
import { EditViewMapComponent } from '../maps/create/edit-view-map.component';
import { MapStudioComponent } from '../maps/studio/map-studio.component';
import { ViewMapComponent } from '../maps/view-map/view-map.component'; 
import { PublicPageComponent } from '../other/public-page/public-page.component';
import { RestrictedPageComponent } from '../other/restricted-page/restricted-page.component';
import { MicrosoftLoginGuard } from '../services/auth/microsoft-login.guard';


const routes: Routes = [{ path: 'public-page', component: PublicPageComponent },  
  { path: 'restricted-page', component: RestrictedPageComponent, canActivate: [MicrosoftLoginGuard] },
  { path: 'blogs/edit/:id', component: EditBlogComponent, canActivate: [MicrosoftLoginGuard] }, 
  { path: 'maps/studio', component: MapStudioComponent, canActivate: [MicrosoftLoginGuard] },
  { path: 'maps/studio/:id', component: MapStudioComponent, canActivate: [MicrosoftLoginGuard] },
  { path: 'maps/:id', component: ViewMapComponent },
  { path: '**', component: HomeComponent }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
