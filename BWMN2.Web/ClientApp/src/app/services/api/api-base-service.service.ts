import { HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MsalService } from '@azure/msal-angular';
import { AuthenticationResult } from '@azure/msal-browser';

@Injectable({
  providedIn: 'root'
})
export class ApiBaseServiceService {

  constructor(private msl: MsalService) { }

  protected async doBaseAction<T>(func: (httpOptions: {}) => Promise<T>) {

    var authResult = await this.acquireAuthToken(); 
    var headerOptions = this.createHeaderOptions(authResult);
    return await func(headerOptions);
  }
 
  private async acquireAuthToken(): Promise<AuthenticationResult> {
    var request = {
      scopes: ["https://bwmeta.onmicrosoft.com/maps-api/API.Access"]
    }

    return await this.msl.acquireTokenSilent(request).toPromise();
  }

  private createHeaderOptions(authResult: AuthenticationResult): {} {

    return {
      headers: new HttpHeaders({
        Authorization: "Bearer " + authResult.accessToken
      })
    };
  }


}
