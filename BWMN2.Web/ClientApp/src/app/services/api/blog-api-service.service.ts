import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { MsalService } from '@azure/msal-angular';
import { BlogModel } from '../../models/blog-model';
import { CreateBlogModel } from '../../models/create-blog-model';
import { ApiBaseServiceService } from './api-base-service.service';

@Injectable({
  providedIn: 'root'
})
export class BlogApiService extends ApiBaseServiceService  {

  constructor(private http: HttpClient, @Inject('BASE_URL') private baseUrl: string, msl: MsalService) {
    super(msl);
  }


  async submitNewBlogAsync(createModel: CreateBlogModel): Promise<BlogModel> {

    return await super.doBaseAction<BlogModel>(async (httpOptions: {}) => {
      return await this.http.post<BlogModel>(this.baseUrl + 'blog/post', createModel, httpOptions).toPromise();
    });
  }


  async saveBlogAsync(model: CreateBlogModel): Promise<void> {

    return await super.doBaseAction<void>(async (httpOptions: {}) => {
      return await this.http.post<void>(this.baseUrl + 'blog/save', model, httpOptions).toPromise();
    });
  }

  async get(id: string): Promise<BlogModel> {

    return await super.doBaseAction<BlogModel>(async (httpOptions: {}) => {
      return await this.http.get<BlogModel>(this.baseUrl + `blog/get?id=${id}&blogType=News`, httpOptions).toPromise();
    });
  }

  async listBlogsAsync(): Promise<BlogModel[]> {
    return await this.http.get<BlogModel[]>(this.baseUrl + 'blog/list').toPromise();
  }

  async deleteBlogAsync(blog: BlogModel): Promise<object> {
    return await super.doBaseAction<object>(async (httpOptions: {}) => {
      return await this.http.delete(this.baseUrl + `blog/delete?id=${blog.id}&blogType=News`, httpOptions).toPromise();
    });
  }
}

class GetBlogModel {
  id: string;
}
