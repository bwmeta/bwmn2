import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { MsalService } from '@azure/msal-angular';
import { AuthenticationResult } from '@azure/msal-browser';
import { inherits } from 'util';
import { CreateMapModel } from '../../models/create-map-model';
import { MapModel } from '../../models/map-model';
import { ApiBaseServiceService } from './api-base-service.service';

@Injectable({
  providedIn: 'root'
})
export class MapsApiService extends ApiBaseServiceService {

  constructor(private http: HttpClient, @Inject('BASE_URL') private baseUrl: string, msl: MsalService) {
    super(msl);
  }


  async submitNewMapAsync(createMapModel: CreateMapModel): Promise<MapModel> {

    return await super.doBaseAction<MapModel>(async (httpOptions: {}) => {
      return await this.http.post<MapModel>(this.baseUrl + 'map/create', createMapModel, httpOptions).toPromise();
    });
  }

  async listMapsAsync(): Promise<MapModel[]> {
    return await this.http.get<MapModel[]>(this.baseUrl + 'map/maps').toPromise();
  }

  async get(id: string): Promise<MapModel> {
    return await this.http.get<MapModel>(this.baseUrl + 'map/get?id=' + id).toPromise();
  }

}
