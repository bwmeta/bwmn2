
import { Injectable } from '@angular/core';
const MAX_WIDTH = 800;
const QUALITY = 0.9;

@Injectable({
  providedIn: 'root'
})
export class CommonImageToolsService {


  async loadFileAsImageDataUrlAsync(file: File): Promise<string> {
    return await new Promise((resolve) => {
      const reader = new FileReader();
      reader.onload = (e) => {

        resolve(reader.result as string);
      }
      reader.readAsDataURL(file);
    });
  }
  async renderDataUrlAsync(img: HTMLImageElement, dataUrl: string) {
    img.src = dataUrl;
    await new Promise((resolve) => {
      img.onload = resolve;
    });
  }

  async resizeImage(file: File): Promise<OptImage> {
    //images only
    if (file.type.match(/image.*/)) {

      var img: HTMLImageElement = document.createElement('img');

      var dataUrl = await this.loadFileAsImageDataUrlAsync(file);
      await this.renderDataUrlAsync(img, dataUrl);

      return new OptImage(img); 
    }
    else {
      throw new Error("only image types allowed");
    }
  };
}

class OptImage {
  public static readonly maxW: number = 800;
  public static readonly maxH: number = 600;
  w: number;
  h: number;

  dataUrl: string;
  constructor(private img: HTMLImageElement) {
    this.w = img.width;
    this.h = img.height;
  }
  getRatio(): number {
    return Math.min(OptImage.maxW / this.w, OptImage.maxH / this.h);
  }
  getCanvasWidth() {
    return this.w * this.getRatio();
  }

  getCanvasHeight() {
    return this.h * this.getRatio();
  }


  toCanvas(): HTMLCanvasElement {
    var canvas = document.createElement("canvas");
    canvas.width = this.getCanvasWidth();
    canvas.height = this.getCanvasHeight();
    var context = canvas.getContext('2d');
    context.drawImage(this.img, 0, 0, this.w * this.getRatio(), this.h * this.getRatio());

    return canvas;
  }
}

