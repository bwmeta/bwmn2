import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AppDataService {

  constructor() { }

  tilesets: TileSet[] = [
    new TileSet(10, "Ash", "ash-icon.jpg"),
    new TileSet(20, "Badlands", "badlands-icon.jpg"),
    new TileSet(30, "Desert", "desert-icon.jpg"),
    new TileSet(40, "Ice", "ice-icon.png"),
    new TileSet(50, "Jungle", "jungle-icon.jpg"),
    new TileSet(60, "Space", "space-icon.jpg"),
    new TileSet(70, "Twilight", "twilight-icon.jpg"),
    new TileSet(80, "Installation", "installation-icon.jpg"),
  ];
  gameTypes: GameType[] = [
    new GameType(10, "1v1"),
    new GameType(20, "2v2"),
    new GameType(30, "3v3"),
    new GameType(40, "4v4"),
    new GameType(50, "2v2v2v2"),
    new GameType(60, "FFA")
  ]
  mapTypes: MapType[] = [
    new MapType(10, "Melee"),
    new MapType(20, "Observer"),
    new MapType(30, "Training"),
    new MapType(40, "No Fog of War")
  ]
  compatibilityModes: CompatibilityMode[] = [
    new CompatibilityMode(10, "1.22+", "1.22+ Broodwar reamaster compliant."),
    new CompatibilityMode(20, "1.16", "1.16 Broodwar compliant.  Necessary for maps with custom AI.")
  ]
  mapStatuses: MapStatus[] = [
    new MapStatus(10, "Alpha/WIP", "Work In Progress and will be continually updated."),
    new MapStatus(20, "Beta/Playtest", "The map is nearing completion and in the testing phase but could change in the future."),
    new MapStatus(30, "Final", "The map is considered finished, it is unlikely the map will change in the future."),
    new MapStatus(40, "Approved", "The map has been recognized as a high quality submission."),
  ]
   
  toMapStatusData(statusId: number): MapStatus {
    return this.mapStatuses.find(a => a.id == statusId);
  }
  toGameTypeData(gameTypeIds: number[]): GameType[] {
    return this.gameTypes.filter(a => gameTypeIds.includes(a.id));
  }
  toCompatibilityData(cId: number): CompatibilityMode {
    return this.compatibilityModes.find(a => a.id == cId);
  }
  toMapTypeData(mapTypeIds: number[]): MapType[] {
    return this.mapTypes.filter(a => mapTypeIds.includes(a.id));
  }
  toTilesetData(tilesetId: number): TileSet {
    return this.tilesets.find(a => a.id == tilesetId);
  }
}




export class TileSet {
  id: number;
  displayName: string;
  thumbnailUrl: string;

  constructor(id: number, name: string, url: string) {
    this.id = id;
    this.displayName = name;
    this.thumbnailUrl = url;
  }
}

export class GameType {
  id: number;
  displayName: string;
  hint: string;

  constructor(id: number, name: string, hint: string = "") {
    this.id = id;
    this.displayName = name;
    this.hint = hint;
  }
}


export class MapType {
  id: number;
  displayName: string;
  hint: string;

  constructor(id: number, name: string, hint: string = "") {
    this.id = id;
    this.displayName = name;
    this.hint = hint;
  }
}


export class CompatibilityMode {
  id: number;
  displayName: string;
  hint: string;

  constructor(id: number, name: string, hint: string) {
    this.id = id;
    this.displayName = name;
    this.hint = hint;
  }
}


export class MapStatus {
  id: number;
  displayName: string;
  hint: string;

  constructor(id: number, name: string, hint:string) {
    this.id = id;
    this.displayName = name;
    this.hint = hint;
  }
}
