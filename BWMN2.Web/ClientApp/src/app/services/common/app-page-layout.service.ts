import { Injectable } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BaseComponent } from '../../common/base/base.component';

@Injectable({
  providedIn: 'root'
})
export class PageLayoutService { 
  pageContainer: string = "container";
  constructor(private route: ActivatedRoute) { }
   
  getComponentLayout(): string {
    return "";
  } 
}
