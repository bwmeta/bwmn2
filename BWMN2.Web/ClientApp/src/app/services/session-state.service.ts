import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SessionStateService {

  private _isLoading: boolean = false;

  constructor() { }

  setIsLoading(value: boolean) {
    this._isLoading = value;
  }

  getIsLoading(): boolean {
    return this._isLoading;
  }
}
