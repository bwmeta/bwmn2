﻿using BWMN2.Web;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.Cosmos;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BWMN.Web.Controllers
{
    [ApiController]
    [Route("[controller]/[action]")]
    [Authorize]
    public class BlogController : ControllerBase
    {  

        public BlogController()
        {

        }

        [HttpPost]
        public async Task<BlogPostModel> Post(BlogPostModel model)
        {
            var user = base.HttpContext.User;
            var claimsId = user.Claims.SingleOrDefault(a => a.Type == "http://schemas.microsoft.com/identity/claims/objectidentifier");
           
            var db = new Db(); 
            await db.InitializeBlogs();

            model.UserId = claimsId.Value;
            model.CreatedDate = DateTime.UtcNow;
            model.ModifiedDate = DateTime.UtcNow;
            model.BlogType = "News";

            await db.Container.CreateItemAsync(model);

            return await Task.FromResult(model); 
        }

            
        [HttpGet]
        public async Task<Blog> Get([FromQuery]string id, [FromQuery]string blogType)
        {
            var user = base.HttpContext.User;
            var claimsId = user.Claims.SingleOrDefault(a => a.Type == "http://schemas.microsoft.com/identity/claims/objectidentifier");

            var db = new Db();
            await db.InitializeBlogs();

            //IQueryable<Blog> queryable = db.Container.GetItemLinqQueryable<Blog>(true);
            
            //queryable = queryable.Where<Blog>(item => item.id == id);
            //var result =  queryable.ToArray().FirstOrDefault();

            var result = await db.Container.ReadItemAsync<Blog>(id, new PartitionKey(blogType));
            return result;
        }

        [HttpPost]
        public async Task Save(BlogPostModel model)
        {
            var user = base.HttpContext.User;
            var claimsId = user.Claims.SingleOrDefault(a => a.Type == "http://schemas.microsoft.com/identity/claims/objectidentifier");

            var db = new Db();
            await db.InitializeBlogs();

            model.UserId = claimsId.Value; 
            model.ModifiedDate = DateTime.UtcNow;
            model.BlogType = "News";

            await db.Container.UpsertItemAsync(model); 
        }

        [HttpDelete]
        public async Task Delete([FromQuery]string id, [FromQuery]string blogType)
        {
            var db = new Db();
            await db.InitializeBlogs();
            await db.Container.DeleteItemAsync<Blog>(id, new PartitionKey(blogType));
        }


        [HttpGet]
        [AllowAnonymous]
        public async Task<List<Blog>> List()
        {
            var db = new Db();
            await db.InitializeBlogs();
            return await db.RetrieveBlogs();
        }
         

        public class BlogPostModel : ModelBase
        {
            public string id { get; set; } = Guid.NewGuid().ToString();
            public string UserId { get; set; }
            public string MessageBody { get; set; }
            public string BlogType { get;  set; }
            public string Subject { get; set; }
        }

        public class ModelBase
        {
            public DateTime CreatedDate { get; set; }
            public DateTime ModifiedDate { get; set; }
        }
    }
}
