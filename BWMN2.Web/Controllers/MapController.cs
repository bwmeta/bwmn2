﻿using BWMN2.Web;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BWMN.Web.Controllers
{
    [ApiController]
    [Route("[controller]/[action]")]
    [Authorize]
    public class MapController : ControllerBase
    {

        public MapController()
        {

        }

        [HttpPost]
        public async Task<MapPostModel> Create(MapPostModel model)
        {
            var db = new Db();
            await db.InitializeMaps();

            var map = new Map()
            {
                id = Guid.NewGuid().ToString(),
                Description = model.Description,
                DisplayName = model.DisplayName,
                Variants = new List<MapVariant>()
                {
                    new MapVariant()
                    {
                        DisplayName = "Competitive Melee",
                        VariantType = 10,
                        Versions = new List<MapVersion>()
                        {
                            new MapVersion()
                            {
                                CompatibilityId = model.Version.CompatibilityId,
                                Description = model.Version.Description,
                                GameTypeIds = model.Version.GameTypeIds,
                                Width = model.Version.Width,
                                Height= model.Version.Height,
                                Id = model.Version.Id,
                                MapFileBlob = model.Version.MapFileBlob,
                                VersionNumber = model.Version.VersionNumber,
                                TilesetId = model.Version.TilesetId,
                                StatusId = model.Version.StatusId,
                                MapTypeIds = model.Version.MapTypeIds,
                                MapThumbnailDataUrl = model.Version.MapThumbnailDataUrl,
                                CreatedDate = DateTime.UtcNow,
                                ModifiedDate = DateTime.UtcNow
                            }
                        }
                    }
                }

            };
            await db.Container.CreateItemAsync(map);

            return await Task.FromResult(model);
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<List<Map>> Maps()
        {
            var db = new Db();
            await db.InitializeMaps();
            return await db.RetrieveMaps();
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<Map> Get([FromQuery] string id)
        {
            var db = new Db();
            await db.InitializeMaps();

            Map map = db.Container.GetItemLinqQueryable<Map>(true)
                                         .Where(item => item.id == id)
                                         .ToList()
                                         .FirstOrDefault();

            return map;
        }
    }

    public class MapPostModel
    {
        public string id { get; set; } = Guid.NewGuid().ToString();
        public string DisplayName { get; set; }
        public string Description { get; set; }
        public string MapThumbnailDataUrl { get; set; }

        public MapVersion Version { get; set; }
    }
}
