﻿using System;
using System.Threading.Tasks;
using System.Configuration;
using System.Collections.Generic;
using System.Net;
using Microsoft.Azure.Cosmos; 
using System.IO;
using Newtonsoft.Json;

namespace BWMN2.Web
{
    public class Db
    {
        // ADD THIS PART TO YOUR CODE

        // The Azure Cosmos DB endpoint for running this sample.
        private static readonly string EndpointUri = "https://bwmeta.documents.azure.com:443/";
        // The primary key for the Azure Cosmos account.
        private static readonly string PrimaryKey = "";

        // The Cosmos client instance
        private CosmosClient cosmosClient;

        // The database we will create
        private Database database;

        // The container we will create.
        public Container Container;

        // The name of the database and container we will create
        private string databaseId = "bwmeta";
        private string _mapsContainerId = "maps";
        private string _blogsContainerId = "blogs";
        // ADD THIS PART TO YOUR CODE
        /*
            Entry point to call methods that operate on Azure Cosmos DB resources in this sample
        */
        public async Task InitializeMaps()
        {
            // Create a new instance of the Cosmos Client
            this.cosmosClient = new CosmosClient(EndpointUri, PrimaryKey);
            this.database = await this.cosmosClient.CreateDatabaseIfNotExistsAsync(databaseId);

            this.cosmosClient = new CosmosClient(EndpointUri, PrimaryKey, new CosmosClientOptions()
            {
                ConnectionMode = ConnectionMode.Gateway
            });

            this.Container = await this.database.CreateContainerIfNotExistsAsync(_mapsContainerId, "/maps");
        }

        public async Task InitializeBlogs()
        {
            // Create a new instance of the Cosmos Client
            this.cosmosClient = new CosmosClient(EndpointUri, PrimaryKey);
            this.database = await this.cosmosClient.CreateDatabaseIfNotExistsAsync(databaseId);

            this.cosmosClient = new CosmosClient(EndpointUri, PrimaryKey, new CosmosClientOptions()
            {
                ConnectionMode = ConnectionMode.Gateway
            });

            this.Container = await this.database.CreateContainerIfNotExistsAsync(_blogsContainerId, "/BlogType");
        } 

        public async Task<List<Map>> RetrieveMaps()
        {
            using (FeedIterator setIterator = Container.GetItemQueryStreamIterator(
                "SELECT * FROM maps"))
            {
                int count = 0;
                while (setIterator.HasMoreResults)
                {
                    using (ResponseMessage response = await setIterator.ReadNextAsync())
                    {

                        count++;
                        using (StreamReader sr = new StreamReader(response.Content))
                        using (JsonTextReader jtr = new JsonTextReader(sr))
                        {
                            var t = sr.ReadToEnd();
                            return JsonConvert.DeserializeObject<CosmosResult<Map>>(t).Documents; 
                        }
                    }
                }
            }

            return new List<Map>();
        }

 
        public async Task<List<Blog>> RetrieveBlogs()
        {
            using (FeedIterator setIterator = Container.GetItemQueryStreamIterator(
               "SELECT * FROM blogs"))
            {
                int count = 0;
                while (setIterator.HasMoreResults)
                {
                    using (ResponseMessage response = await setIterator.ReadNextAsync())
                    {

                        count++;
                        using (StreamReader sr = new StreamReader(response.Content))
                        using (JsonTextReader jtr = new JsonTextReader(sr))
                        {
                            var t = sr.ReadToEnd();
                            return JsonConvert.DeserializeObject<CosmosResult<Blog>>(t).Documents;
                        }
                    }
                }
            }

            return new List<Blog>();
        }
    }

    public class Map
    {
        public string id { get; set; } 
        public string DisplayName { get; set; }
        public string Description { get; set; }

        public List<MapVariant> Variants { get; set; }

        public string _rid { get; set; }
        public string _self { get; set; }
        public string _etag { get; set; }
        public string _attachments { get; set; }
        public string _ts { get; set; }
    }

    public class MapVariant
    {
        public string DisplayName { get; set; }
        public int VariantType { get; set; }
        public List<MapVersion> Versions { get; set; }
    }
    public class MapVersion
    {
        public string Id { get; set; }
        public string MapThumbnailDataUrl { get; set; }
        public string MapFileBlob { get; set; }
        public float VersionNumber { get; set; }
        public string Description { get; set; }
        public int TilesetId { get; set; }
        public List<int> GameTypeIds { get; set; }
        public List<int> MapTypeIds { get; set; }
        public int StatusId { get; set; }
        public int CompatibilityId { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }

    }
    public class Blog
    {
        public string Subject { get; set; }
        public string MessageBody { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string UserId { get; set; }
        public string id { get; set; }
    }
    public class CosmosResult<T>
    {
        public string _rid { get; set; }
        public string _count { get; set; }

        public List<T> Documents = new List<T>();
    }
}
