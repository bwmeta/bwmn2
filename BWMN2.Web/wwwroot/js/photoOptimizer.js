const MAX_WIDTH = 800;
const QUALITY = 0.9;

const readPhoto = async (instance) => {
    const canvas = document.createElement('canvas');
    const img = document.createElement('img');
    const file = document.querySelector('input[type=file]').files[0];


    // create img element from File object
    img.src = await new Promise((resolve) => {
        const reader = new FileReader();
        reader.onload = (e) => { resolve(e.target.result); }
        reader.readAsDataURL(file);
    });
    await new Promise((resolve) => {
        img.onload = resolve;
    });

    let maxW = 800;
    let maxH = 600;
    let w = img.width;
    let h = img.height;

    let ratio = Math.min(maxW / w, maxH / h);
    canvas.width = w * ratio;
    canvas.height = h * ratio;

    var context = canvas.getContext('2d');
    context.drawImage(img, 0, 0, w * ratio, h * ratio);
    let d = context.getImageData(0, 0, w * ratio, h * ratio);

    console.log(d); 
    canvas.toBlob((blob) => {
        var newimg = document.getElementById("testImage");
        newimg.src = URL.createObjectURL(blob);
        
    })

    return;
};
 

window.imageService = (function () {
    return {
        readPhoto: readPhoto
    }
})();